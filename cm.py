#!/usr/bin/env python3



# (c) Marcin Jurczuk <marcin@jurczuk.eu>
import argparse
import re
from easysnmp import Session


OID_docsDevSwServer=".1.3.6.1.2.1.69.1.3.1.0"
OID_docsDevSwFilename=".1.3.6.1.2.1.69.1.3.2.0"
# docsDevSwServerTransportProtocol 1 - tftp, 2 - http
OID_docsDevSwServerTransportProtocol=".1.3.6.1.2.1.69.1.3.8.0"
OID_docsDevSwAdminStatus=".1.3.6.1.2.1.69.1.3.3.0"

RE_SYSDESC=r'(.*<<)(.*)>>'

## command possible arguments
def arguments():
    parser = argparse.ArgumentParser(description='Cable modem querier')
    subparsers = parser.add_subparsers(help='sub-command help',dest='subopt_name')
    parser_version = subparsers.add_parser('version', help='get cable modem version information')
    parser_upgrade= subparsers.add_parser('upgrade', help='get cable modem version information')
    parser_logs = subparsers.add_parser('logs', help='get cable modem logs')
    parser_version.add_argument('-c','--community', help='SNMP community used to make query',default="public")
    parser_logs.add_argument('-c','--community', help='SNMP community used to make query',default="public")
    parser_upgrade.add_argument('-c','--community', help='SNMP community used to make query',default="private")
    parser_upgrade.add_argument('-s','--server', help='TFTP/HTTP server IP address',default="private")
    parser_upgrade.add_argument('-f','--filename', help='TFTP/HTTP server filename path',default="private")
    parser_version.add_argument('cmip',help="Cable modem ip address")
    parser_logs.add_argument('cmip',help="Cable modem ip address")
    parser_upgrade.add_argument('cmip',help="Cable modem ip address")
    vals = parser.parse_args()
    return vals

## retrieve sysDesct.0 where modems presents all basin info
def get_cm_version(hostname,community,snmp_version=2):
    try:
        session = Session(hostname=hostname, community=community, version=snmp_version)
        sysdesc = session.get('sysDescr.0')
        return sysdesc.value
    except Exception as e:
        print("get_cm_version() Error: {}".format(e))
        return ""

## cable modem event log
def get_cm_logs(hostname,community,snmp_version=2):
    session = Session(hostname=hostname, community=community, version=snmp_version)
    logs = session.walk('DOCS-CABLE-DEVICE-MIB::docsDevEvText')
    for log in logs:
        print("{}".format(log.value))

## upgrade cm - set few oind and here we go 
def upgrade_cm(hostname,community,tftp_server, file_path):
    session = Session(hostname=hostname, community=community, version=2)
    try:
        session.set("DOCS-CABLE-DEVICE-MIB::docsDevSwServer.0",tftp_server)
        session.set("DOCS-CABLE-DEVICE-MIB::docsDevSwFilename.0",file_path)
        session.set("DOCS-CABLE-DEVICE-MIB::docsDevSwAdminStatus.0",1)
        print("Upgrade in progress, check status via modem SNMP logs")
    except Exception as e:
        print("upgrade_cm() Error: {}".format(e))
        sys.exit(0)


## main()
if __name__ == "__main__":
    args = arguments()
    if args.subopt_name == 'version':
        pdu=get_cm_version(args.cmip, args.community)
        m = re.match(RE_SYSDESC,pdu)
        if m:
            print("{0}".format(m[2]))
        else:
            print("WARNING: Device is not a modem !")
            print(pdu.value)
    if args.subopt_name == "upgrade":
        upgrade_cm(args.cmip, args.community,args.server, args.filename)
    if args.subopt_name == "logs":
        get_cm_logs(args.cmip, args.community)

